package conversion

import java.util.Date

import currency.filter.{MatchingCurrencyFilter, MatchingDateFilter}
import currency.{AvailableMultipliers, Currency, CurrencyMultiplier, Money}

case class CurrencyConverterBuilder(baseCurrency: Currency, availableMultipliers: AvailableMultipliers) {

    private var destinationCurrency: Option[Currency] = None

    private var conversionDate: Date = new Date()

    def destinationCurrency(currency: Currency): CurrencyConverterBuilder = {
        this.destinationCurrency = Some(currency)
        this
    }

    def at(conversionDate: Date): CurrencyConverterBuilder = {
        this.conversionDate = conversionDate
        this
    }

    def convert(money: Money): Money = {
        if (destinationCurrency.isEmpty)
            throw new IllegalArgumentException(s"Cannot convert because destination currency was not specified")

        val matchingMultipliers =
            availableMultipliers
                .applyFilter(new MatchingCurrencyFilter(money.currency, destinationCurrency.get))
                .applyFilter(new MatchingDateFilter(conversionDate))

        matchingMultipliers.get match {
            case Nil =>
                findIndirectMultiplier(money.currency, destinationCurrency.get) match {
                    case Some(multiplier) => multiplier.apply(money)
                    case None => throw new IllegalArgumentException(s"No matching multiplier found")
                }
            case single :: Nil => single.apply(money)
            case multiple => throw new IllegalArgumentException(s"Multiple matching multipliers found")
        }
    }

    private def findIndirectMultiplier(sourceCurrency: Currency, destinationCurrency: Currency): Option[CurrencyMultiplier] = {
        availableMultipliers
            .applyFilter(new MatchingCurrencyFilter(sourceCurrency, baseCurrency))
            .applyFilter(new MatchingCurrencyFilter(baseCurrency, destinationCurrency))
            .get()
            .headOption
    }

}
