package currency.filter

import java.util.Date

import currency.CurrencyMultiplier

class MatchingDateFilter(date: Date) extends MultiplierFilter {

    override def apply(multipliers: Seq[CurrencyMultiplier]): Seq[CurrencyMultiplier] = {
        multipliers.filter(_.effectiveDate.after(date))
    }
}
