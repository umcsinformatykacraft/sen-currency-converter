package currency

class CurrencyProvider {

    private val supportedCurrencies: List[Currency] = readInitialCurrencies

    def getSupportedCurrencies() = supportedCurrencies

//    def find(currency: Currency): Option[Currency] = supportedCurrencies.find(_ == currency)
//
//    def find(currency: String): Option[Currency] =
//        supportedCurrencies.find(_.code.equalsIgnoreCase(StringUtils.trim(currency)))


    private def readInitialCurrencies: List[Currency] = {
        //TODO: Read from db
        List(
            new Currency("PLN"),
            new Currency("EUR"),
            new Currency("USD")
        )
    }
}
