package currency

import java.util.Date

case class CurrencyMultiplier(sourceCurrency: Currency, destinationCurrency: Currency, ratio: Double, effectiveDate: Date) {

    def apply(money: Money): Money = {

        money.currency.code match {
            case sourceCurrency.code => Money(money.amount, destinationCurrency)
            case literallyAnyOther => throw new IllegalArgumentException(s"This multipler cannot handle $literallyAnyOther")
        }
    }

    private[currency] def calculateNewAmount(inputAmount: Double, ratio: Double) = inputAmount * ratio
}
