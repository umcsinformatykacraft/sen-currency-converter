package conversion

import java.util.Date

import currency.{CurrencyMultiplier, _}

object main {

    def main(args: Array[String]): Unit = {

        val currencyProvider = new CurrencyProvider

        val userClickedMoney = Money(31.3, Currency("EUR"))

        val multipliers = new AvailableMultipliers(
            CurrencyMultiplier(Currency("PLN"), Currency("EUR"), ratio = 4.0f, new Date()),
            CurrencyMultiplier(Currency("EUR"), Currency("PLN"), ratio = 0.4f, new Date()),
            CurrencyMultiplier(Currency("USD"), Currency("EUR"), ratio = 0.8f, new Date()),
            CurrencyMultiplier(Currency("EUR"), Currency("USD"), ratio = 1.2f, new Date())
        )


        val moneyAfterConversion =
            CurrencyConverterBuilder(Currency("PLN"), multipliers)
                .destinationCurrency(Currency("EUR"))
                .at(new Date())
                .convert(userClickedMoney)

        println(moneyAfterConversion)

    }

}
