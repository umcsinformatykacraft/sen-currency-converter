package currency.filter

import java.util.Date

import currency.{Currency, CurrencyMultiplier}
import org.scalatest.{FreeSpec, Matchers}

class MatchingCurrencyFilterSpec extends FreeSpec with Matchers {

    "given source and destination currency" - {
        val sourceCurrency = Currency("PLN")
        val destinationCurrency = Currency("USD")
        val anotherCurrency = Currency("EUR")

        val filter = new MatchingCurrencyFilter(sourceCurrency, destinationCurrency)

        "should filter only multipliers for the same currencies" in {
            filter.apply(Seq(
                testableMultiplier(sourceCurrency, destinationCurrency),
                testableMultiplier(destinationCurrency, sourceCurrency),
                testableMultiplier(anotherCurrency, destinationCurrency),
                testableMultiplier(sourceCurrency, anotherCurrency)
            )) shouldBe testableMultiplier(sourceCurrency, destinationCurrency)
        }
    }

    private def testableMultiplier(sourceCurrency: Currency, destinationCurrency: Currency) =
        CurrencyMultiplier(sourceCurrency, destinationCurrency, 0.0f, new Date)
}
