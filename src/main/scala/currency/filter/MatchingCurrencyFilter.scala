package currency.filter

import currency.{Currency, CurrencyMultiplier}

class MatchingCurrencyFilter(sourceCurrency: Currency, destinationCurrency: Currency) extends MultiplierFilter {
    override def apply(multipliers: Seq[CurrencyMultiplier]): Seq[CurrencyMultiplier] = {
        multipliers.filter(multiplier => {
            multiplier.sourceCurrency == sourceCurrency &&
            multiplier.destinationCurrency == destinationCurrency
        })
    }
}
