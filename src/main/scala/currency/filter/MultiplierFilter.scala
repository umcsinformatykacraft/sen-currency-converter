package currency.filter

import currency.CurrencyMultiplier

trait MultiplierFilter {
    def apply(multipliers: Seq[CurrencyMultiplier]): Seq[CurrencyMultiplier]
}
