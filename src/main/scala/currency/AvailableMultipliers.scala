package currency

import currency.filter.MultiplierFilter

class AvailableMultipliers(currencyMultipliers: CurrencyMultiplier*) {

    private val availableMultipliers = currencyMultipliers

    def applyFilter(multiplierFilter: MultiplierFilter): AvailableMultipliers =
        new AvailableMultipliers(multiplierFilter.apply(availableMultipliers) :_*)

    def get(): Seq[CurrencyMultiplier] = availableMultipliers
}
